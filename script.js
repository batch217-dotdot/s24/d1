// Section - Exponent Operator

const firstNum = 8 ** 2;
console.log(firstNum);

// After ES6 Update
const secondNum = Math.pow(8, 2);
console.log(secondNum);

const thirdNum = Math.pow(8, 3);
console.log(thirdNum);

// Section - Template Literals
let name = "John";

// pre-template literal strings
let message = "Hello " + name + "! Welcome to programming!";
console.log("Message without template literals: " + message);

// Using template literal
// we use backticks (``)
message = `Hello ${name}! Welcome to programming!`;
console.log(`Message with template literals: ${message}`);

// Multi-line using template literals
const anotherMessage = `${name} attended a math competition.
He won it by solving the problem 8 ** 2 with solution of ${firstNum}.`;
console.log(anotherMessage);

const interestRate = .1;
const pricipal = 1000;

console.log(`The interest on your savings account is: ${pricipal * interestRate}`);

// Section - Array Destructuring

const fullName = ["Juan", "Dela", "Cruz"];

// Pre-array Destructuring
console.log(fullName[0]); // Juan
console.log(fullName[1]); // Dela
console.log(fullName[2]); // Cruz

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`);

// Array Destructuring
const [firstName, middleName, lastName] = fullName;

// Using template literal
console.log(`Hello ${firstName} ${middleName} ${lastName}`);

// Object Destructuring
const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}

// Pre-object destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to you.`);

// Object destructuring
const {givenName, maidenName, familyName} = person;
console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to you again.`);

// using destructure variable to a function
function getFullName({givenName, maidenName, familyName}) {
	console.log(`${givenName} ${maidenName} ${familyName}`);
}
getFullName(person);

// Arrow Function
/*
syntax
const variableName = () => {
	code block
}
*/

const hello = () => {
	console.log("Hello World!");
}
hello();

// Traditional function

/*function printFullName(firstName, middleName, lastName) {
	console.log(firstName + " " + middleName + " " + lastName);
}
printFullName("John", "Doe", "Smith");*/

// Arrow Function
const printFullName = (firstName, middleName, lastName) => {
	console.log(`${firstName} ${middleName} ${lastName}`);
}

printFullName("John", "Doe", "Smith");

const students = ["John", "Jane", "Judy"];

// Arrow function with loops
// pre-arrow function
students.forEach(function(student) {
	console.log(`${student} is a student`);
})

// arrow function
students.forEach((student) => {
	console.log(`${student} is a student`);
});

// Section - Default function argument value

const greet = (name = "User") => {
	return `Good morning, ${name}`
}
console.log(greet());
console.log(greet("Russel"));
console.log(greet());


// Section - Class-based Object blueprints

// Creating a class

class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

const myCar = new Car();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar);

const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);